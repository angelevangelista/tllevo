Este proyecto consiste en un sistema de carpooling para utilizarse en la región 
de  Santo Domingo funcionando como multiplataforma de iOS y Android. 

La app está siendo desarrollada en Xamarin Forms, utilizando la arquitectura MVVM
y una base de datos no relacional (Firebase).

