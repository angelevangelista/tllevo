﻿using Firebase.Database;
using Firebase.Database.Query;
using System;
using System.Linq;
using System.Threading.Tasks;
using TLlevo.Models;

namespace TLlevo
{
    public class LoginGen
    {
        FirebaseClient firebase = new FirebaseClient("https://fabuloso-bd00f.firebaseio.com");

        public  async Task AddUser(Login login)
        {
            try
            {
                await firebase
                 .Child("Login")
                 .PostAsync(new Login() { correo = login.correo, password = login.password, cedula = login.cedula });
            }
            catch (Exception p)
            {

                throw;
            }

           
        }

        public async Task<bool> ConsultUserAsync (Login login)
        {
            bool exit = (await firebase
             .Child("Login")
             .OnceAsync<Login>()).Where(a => a.Object.correo == login.correo && a.Object.password==login.password).Any();

            return exit;
        }
    }
}
