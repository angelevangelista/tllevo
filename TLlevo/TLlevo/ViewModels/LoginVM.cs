﻿using System;
using System.Threading.Tasks;
using TLlevo.Models;
using TLlevo.Views;
using Xamarin.Forms;

namespace TLlevo.ViewModel
{
    public class LoginVM: Login
    {
        public INavigation Navigation { get; set; }
        public Login Login { get; set; }        
        LoginGen instancia = new LoginGen();
        public Command CreateCommand { get; set; }
        public Command ConsultCommand{ get; set; }
        public Command RegisterCommand { get; set; }
        public LoginView LoginView { get; set; }

        

        public LoginVM(INavigation navigation=null, Login login = null, LoginView page=null)
        {
            Login= login ?? new Login();
            CreateCommand = new Command(async () => await Save(),()=> !Isbusy);
            ConsultCommand = new Command(async () => await Consult(), () => !Isbusy);
            Navigation = navigation;
            LoginView = page;
        }

        private async Task Save()
        {
                Isbusy = true;
                await instancia.AddUser(Login);
                Isbusy = false;
        }

        private async Task Consult()
        {
            Isbusy = true;
            bool existe = await instancia.ConsultUserAsync(Login);
            Isbusy = false;
            /*if (existe) await Navigation.PushAsync(new Views.Principal.SegmentoCentral.MainPage());
            else await LoginView.DisplayAlert("Error", "Este usuario no existe en nuestro registro", "Ok");*/

        }

    }
}
