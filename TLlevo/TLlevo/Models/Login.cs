﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace TLlevo.Models
{
   public class Login: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperyChanged([CallerMemberName]string parametro=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(parametro));
        }

        #region Propiedades vacías

        private int _idUsuario = 0;
        private string _password = string.Empty;
        private string _cedula= string.Empty;
        private string _correo = string.Empty;
        private bool _isbusy= false;
        #endregion

        public int idUsuario
        {

            get => _idUsuario;
            set { _idUsuario = value; OnProperyChanged(); }
        }

        public string password
        {
            get => _password;
            set { _password = value; OnProperyChanged(); }
        }

        public string cedula
        {
            get => _cedula;
            set { _cedula = value; OnProperyChanged(); }
        }

        public bool Isbusy
        {
            get => _isbusy;
            set { _isbusy = value; OnProperyChanged(); }
        }


        public string correo
        {
            get => _correo;
            set { _correo = value; OnProperyChanged(); }

        }
    }
}
