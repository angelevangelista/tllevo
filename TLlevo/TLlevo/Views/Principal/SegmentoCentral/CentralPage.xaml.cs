﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TLlevo.Views.Principal.SegmentoCentral
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CentralPage : ContentPage
	{
		public CentralPage ()
		{
			InitializeComponent ();
		}
	}
}