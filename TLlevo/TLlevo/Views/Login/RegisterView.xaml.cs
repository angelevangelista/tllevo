﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLlevo.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TLlevo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterView : ContentPage
	{
        LoginVM context = new LoginVM();

        public RegisterView ()
		{
			InitializeComponent ();
            BindingContext = context;
		}
	}
}