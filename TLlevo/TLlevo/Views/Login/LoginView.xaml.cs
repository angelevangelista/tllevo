﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TLlevo.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TLlevo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : ContentPage
	{
        public LoginView ()
		{
            InitializeComponent ();
            BindingContext = new LoginVM(Navigation,null,this);
        }

        private void BtnRedirect(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterView());
        }

    }
}