﻿using System;
using TLlevo.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TLlevo
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new CustomNavigationPage(new Views.Principal.SegmentoCentral.CentralPage());
            //MainPage = new NavigationPage(new Views.Principal.SegmentoCentral.CentralPage() { BackgroundColor= Color.Transparent});

           /*MainPage = new NavigationPage(new Views.Principal.SegmentoCentral.CentralPage())
            {
                BarBackgroundColor = Color.Transparent
            };*/

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
